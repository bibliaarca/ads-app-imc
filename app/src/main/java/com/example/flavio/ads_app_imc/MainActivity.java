package com.example.flavio.ads_app_imc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public double calculaIMC (double altura, double peso) {
        Double imc = peso /(altura * altura);
        DecimalFormat f = new DecimalFormat("#.####");
        return Double.parseDouble(f.format(imc));
    }
    public void calculaImc(View view) {

        EditText edtpeso = (EditText) findViewById(R.id.EditTextPeso);
        EditText edtaltura = (EditText) findViewById(R.id.EditTextAltura);
        TextView imc =  (TextView) findViewById(R.id.TextViewResultadoIMC);
        double peso = Double.parseDouble(edtpeso.getText().toString());
        double altura = Double.parseDouble(edtaltura.getText().toString());
        double calcimc = calculaIMC(altura, peso);
        imc.setText(  String.format("%.2f", calcimc) );

    }

    public void creditosOpen(View view) {

          Intent i = new Intent(this, Activity_B.class);
          startActivity(i);

    }

}
